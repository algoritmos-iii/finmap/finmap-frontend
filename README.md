# Finmap-FronEnd

Repositório para o Front da aplicação Finmap, elaborada na disciplina de Algoritmos e Programação III do curso de Análise e Desenvolvimento de Sistemas da Faculdade Senac Pelotas. 

Grupo formado por:

| **Integrante** | **Email** | **GitLab** | **GitHub** | **Função** |
| :------------: | :-------: | :--------: | :--------: | :--------: |
| Paulo Henrique Hartwig Junior | paulohhartwig@gmail.com | [Perfil](https://gitlab.com/PauloHartwig) | [Perfil](https://github.com/PauloHartwig) | API |
| Vitor Hugo Sell Rodrigues | vitorsellrs@gmail.com | [Perfil](https://gitlab.com/VitorHSR)| [Perfil](https://github.com/VitorHSR) | Front |
| Igor Luiz Silva | igorsillva@hotmail.com.br | [Perfil](https://gitlab.com/Maartyr) | x | Front |

## Rotas

### Postman

Segue abaixo a documentação das rotas para acessar as funcionalidades básicas da API.

[Documentação das rotas da API.](https://documenter.getpostman.com/view/4145998/S1TVVcLY)

### Frontend Rotas

As rotas do front estão todas especificadas [nessa issue](https://gitlab.com/algoritmos-iii/finmap/finmap-frontend/issues/14).

## How to Start

### Clone e Download

Clone via SSH:

`git@gitlab.com:algoritmos-iii/finmap/finmap-frontend.git`

Clone via HTTP:

`https://gitlab.com/algoritmos-iii/finmap/finmap-frontend.git`

Além disto, é possível baixar o projeto em outros formatos clicando na opção "Download".

### IDE

A aplicação foi desenvolvida utilizando a IDE Visual Studio Code. Caso queira fazer alguma mudança sem fazer alterações em outros arquivos, é recomendado que utilize a mesma IDE de desenvolvimento. 

## Heroku

O Front está disponível em https://finmap-frontend.herokuapp.com/ + as rotas especificadas [nessa issue](https://gitlab.com/algoritmos-iii/finmap/finmap-frontend/issues/14).
