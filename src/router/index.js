import Vue from 'vue'
import Router from 'vue-router'
import TesteTipo from '@/components/TesteTipo'
import TesteUsuario from '@/components/TesteUsuario'
import TesteConta from '@/components/TesteConta'
import TesteGrafico from '@/components/TesteGrafico'
import TesteLancamento from '@/components/TesteLancamento'
import TesteMeta from '@/components/TesteMeta'
import Cadastro from '@/components/GeneralViews/Cadastro'
import Conta from '@/components/GeneralViews/Conta'
import Grafico from '@/components/GeneralViews/Grafico'
import Home from '@/components/GeneralViews/Home'
import Lancamento from '@/components/GeneralViews/Lancamento'
import Login from '@/components/GeneralViews/Login'
import Meta from '@/components/GeneralViews/Meta'
import Tipo from '@/components/GeneralViews/Tipo'
import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue);

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/testeTipo',
      name: 'TesteTipo',
      component: TesteTipo
    },
    {
      path: '/testeUsuario',
      name: 'TesteUsuario',
      component: TesteUsuario
    },
    {
      path: '/testeConta',
      name: 'TesteConta',
      component: TesteConta
    },
    {
      path: '/testeGrafico',
      name: 'TesteGrafico',
      component: TesteGrafico
    },
    {
      path: '/testeLancamento',
      name: 'TesteLancamento',
      component: TesteLancamento
    },
    {
      path: '/testeMeta',
      name: 'TesteMeta',
      component: TesteMeta
    },
    {
      path: '/cadastro',
      name: 'Cadastro',
      component: Cadastro
    },
    {
      path: '/conta',
      name: 'Conta',
      component: Conta
    },
    {
      path: '/grafico',
      name: 'Grafico',
      component: Grafico
    },
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/lancamento',
      name: 'Lancamento',
      component: Lancamento
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/meta',
      name: 'Meta',
      component: Meta
    },
    {
      path: '/tipo',
      name: 'Tipo',
      component: Tipo
    },
  ]

})
