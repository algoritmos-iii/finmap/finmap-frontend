import axios from 'axios'

export const http = axios.create({
    baseURL: 'https://finmap-api.herokuapp.com/'
})