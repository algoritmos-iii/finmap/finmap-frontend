import { http } from './config'

export default {

    listar:() => {
        return http.get('meta')
    },

    salvar:(meta) => {
        return http.post('meta', meta)
    },

    atualizar:(meta) => {
        return http.put('meta', meta)
    },

    apagar:(meta) => {
        return http.delete('meta', { data: meta })
    }

}